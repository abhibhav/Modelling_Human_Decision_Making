# Code for computing the Gittins Index for a Multi-Armed Bandit problem
# Assuming Beta Priors and Bernoulli distributed rewards

"""
We will use Memoization and use a binary search to find
the right value of lambda
The recursion used is the following:
U(lambda, pi) = max(lambda / (1 - gamma), r(pi) + integral(U'(lambda, pi_x) f(x|pi) dx))
See Multi-armed Bandit Allocation Indices, John Gittins for details
"""

import numpy as np
import random

# Library that makes a pretty progress bar
from tqdm import tqdm

import matplotlib.pyplot as plt

def abs(a):
    """Helper function that returns absolute value
    """
    if a > 0:
        return a
    return -1 * a


def index(alpha, beta, gamma, steps, horizon):
    """ Function that calculates the Gittins Index
    for given alpha, using horizon as the look ahead limit
    args:
        alpha, beta: Beta distribution papers
        gamma: Discount factor
        step: Steps in which we should create the grid to look for the Index
        horizon: The limit upto which the index is calculated
    """
    
    # horizon+1 just to make the indexing easier, Beta distribution requires
    # alpha, beta > 0
    temp = np.zeros([horizon + 1, horizon + 1])

    alphaOffset = alpha - 1
    betaOffset = beta - 1
    totalOffset = alphaOffset + betaOffset

    GittinsIndex = None
    GittinsError = None

    #Initialize limiting values to just be reward
    for i in range(1, horizon):
        temp[i][horizon - i] = (i + alphaOffset) / (horizon + totalOffset)
    
    poss = np.arange(0, 1, steps)
    start = 0
    end = len(poss)

    while True:
        # Binary search through the parameter space, while storing the best index
        # Works since the standard arm increases faster as a function of lambda
        mid = int((start + end) / 2)
        lam = poss[mid]

        # Find the total discounted reward for the standard arm
        standardArm = lam / (1 - gamma)

        for total in range(horizon - 1, 1, -1):
            for tAlpha in range(1, total):
                tBeta = total - tAlpha
                
                # Find out the reward for pulling the arm
                arm = ((tAlpha + alphaOffset) / (total + totalOffset)) * (1 + gamma * temp[tAlpha + 1, tBeta]) + ((tBeta + betaOffset) / (total + totalOffset)) * (gamma * temp[tAlpha, tBeta + 1])
                temp[tAlpha, tBeta] = max(standardArm, arm)
         
        # arm is used since at the end of the loops, it stores the value for the required alpha, beta
        if (GittinsIndex == None) or (GittinsError > abs(arm - standardArm)):
            GittinsIndex = lam
            GittinsError = abs(arm - standardArm)
        
        if standardArm > arm:
            end = mid - 1
        else:
            start = mid + 1

        if start >= end:
            break
                
    return GittinsIndex


def index_all_upto_n(gamma, steps, n):
    """ Function that calculates the Gittins Index
    for all alpha, beta upto alpha + beta = n
    args:
        alpha, beta: Beta distribution papers
        gamma: Discount factor
        step: Steps in which we should create the grid to look for the Index
        n: The limit upto which the index is calculated
    """
    
    # n+1 just to make the indexing easier, Beta distribution requires
    # alpha, beta > 0
    temp = np.zeros([n+1, n+1])
    Gittins = np.zeros([n+1, n+1]) - 1

    #Initialize limiting values to just be reward
    for i in range(1, n):
        temp[i][n-i] = i / n
    

    for lam in tqdm(np.arange(0, 1, steps)):
        # For a grid of values of lambda, calculate temp

        # Find the total discounted reward for the standard arm
        standardArm = lam / (1 - gamma)

        for total in range(n-1, 1, -1):
            for tAlpha in range(1, total):
                tBeta = total - tAlpha
                
                # Find out the reward for pulling the arm
                arm = (tAlpha / total) * (1 + gamma * temp[tAlpha + 1, tBeta]) + (tBeta / total) * (gamma * temp[tAlpha, tBeta + 1])
                
                if (Gittins[tAlpha, tBeta] == -1) and (standardArm > arm):
                    Gittins[tAlpha, tBeta] = lam;

                temp[tAlpha, tBeta] = max(standardArm, arm)
                
    return Gittins

